# Inquiry Admin Frontend
### A vue app to administrate the request API.

## Prerequisites:
- Git
- Docker
- Docker-Compose
## Using
- Vue
- VueRouter
- Vuex
- Vuelidate
- Vue-Notification
- Vue-Bootstrap
- Bootstrap
- Axios

## Installation
`
git clone http://git.b-33.de/leo/b33frontente.git
`

## Development

Build the image:

`
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
`

## Stage
Build the image:

`
docker-compose -f docker-compose.yml -f docker-compose.stage.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml -f docker-compose.stage.yml up -d
`


## Production

Build the image:

`
docker-compose -f docker-compose.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml up -d
`