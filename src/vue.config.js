module.exports = {
    chainWebpack: (config) => {
      config.resolve.symlinks(false)
    },
    devServer: {
      disableHostCheck: true,
      host: '0.0.0.0',
    },
  }