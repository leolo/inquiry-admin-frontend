import Vue from 'vue';
import VueRouter from 'vue-router';
import InquiryList from '../components/InquiryList.vue';
import InquiryForm from '../components/InquiryForm.vue';
import ArchivedInquiryList from '../components/ArchivedInquiryList.vue';
import Login from '../components/Login.vue';
import User from '../components/User.vue';
import Users from '../components/Users.vue';
import PrivacyPolicy from '../components/PrivacyPolicy.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/inquiries',
    component: InquiryList,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/archived_inquiries',
    component: ArchivedInquiryList,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/inquiry',
    component: InquiryForm,
  },
  {
    path: '/user/me',
    component: User,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/users',
    component: Users,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/login',
    component: Login,
    alias: '/',
  },
  {
    path: '/privacy_policy',
    component: PrivacyPolicy,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!Vue.$cookies.isKey('auth')) {
      next({
        path: '/login',
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});
export default router;
