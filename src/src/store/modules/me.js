import axios from 'axios';
import {
  getHeader, notificationHandler, notificationErrorHandler, setAuthCookie, logOut, getRootUrl,
} from './util';

/*
eslint no-shadow: ["error", { "allow": ["state"] }]
*/
const state = {
  me: [],
};

const getters = {
  getMe: (state) => state.me,
};

const actions = {
  /* eslint no-unused-vars: [2, { "args": "none" }] */
  login({ commit }, bodyFormData) {
    return new Promise((resolve, reject) => {
      const path = `${getRootUrl()}/token`;
      axios.post(path, bodyFormData)
        .then((response) => {
          setAuthCookie(response);
          resolve();
        })
        .catch((e) => {
          notificationErrorHandler(e);
          reject();
        });
    });
  },
  fetchMe({ commit }) {
    const config = getHeader();
    axios.get(`${getRootUrl()}/users/me`, config)
      .then((response) => {
        commit('setMe', response.data);
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  editMe({ commit }, me) {
    const config = getHeader();
    axios.put(`${getRootUrl()}/users/me`, me, config)
      .then((response) => {
        commit('setMe', response.data);
        notificationHandler('Eigenen Benutzer aktualisiert', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  deleteMe({ commit }) {
    const config = getHeader();
    axios.delete(`${getRootUrl()}/users/me`, config)
      .then(() => {
        commit('removeMe');
        logOut();
        notificationHandler('Eigenen Benutzer gelöscht', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
};

const mutations = {
  setMe: (state, me) => { state.me = me; },
  removeMe: (state) => { state.me = []; },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
