import axios from 'axios';
import {
  getHeader, notificationHandler, notificationErrorHandler, getRootUrl,
} from './util';

const methods = {
  formatInquiry(obj) {
    const newObj = obj;
    newObj.person.fullName = `${obj.person.name}, ${obj.person.prename}`;
    newObj.person.created_at = this.formatDateTime(new Date(obj.person.created_at));
    newObj.person.updated_at = this.formatDateTime(new Date(obj.person.updated_at));
    newObj.inquiry.created_at = this.formatDateTime(new Date(obj.inquiry.created_at));
    newObj.inquiry.updated_at = this.formatDateTime(new Date(obj.inquiry.updated_at));
    newObj.inquiry.arrivalLocal = this.formatDate(new Date(obj.inquiry.arrival));
    newObj.inquiry.departureLocal = this.formatDate(new Date(obj.inquiry.departure));
    newObj.inquiry.formattedRooms = this.formatRooms(obj.inquiry);
    newObj.inquiry.formattedPersonNumbers = this.formatPersonNumbers(obj.inquiry);
    return newObj;
  },
  formatDateTime(date) {
    return `${date.toLocaleString()}`;
  },
  formatDate(date) {
    return `${date.toLocaleDateString()}`;
  },
  formatRooms(inquiry) {
    const north = Number(inquiry.roomNorth);
    const south = Number(inquiry.roomSouth);
    let formatedRooms = '';
    if (north && south) {
      formatedRooms = 'Nord, Süd';
    } else if (north) {
      formatedRooms = 'Nord';
    } else if (south) {
      formatedRooms = 'Süd';
    }
    return formatedRooms;
  },
  formatPersonNumbers(inquiry) {
    return Number(inquiry.numberAdults) + Number(inquiry.numberReduced);
  },
};

/*
eslint no-shadow: ["error", { "allow": ["state"] }]
*/
const state = {
  inquiries: [],
};

const getters = {
  allInquiries: (state) => state.inquiries,
};

const actions = {
  fetchInquiries({ commit }) {
    const objs = [];
    const config = getHeader();
    axios.get(`${getRootUrl()}/inquiries`, config)
      .then((response) => {
        response.data.forEach((obj) => {
          objs.push(methods.formatInquiry(obj));
        });
        commit('setInquiries', objs);
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  addInquiry({ commit }, form) {
    return new Promise((resolve, reject) => {
      axios.post(`${getRootUrl()}/inquiries`, form)
        .then((response) => {
          commit('newInquiry', methods.formatInquiry(response.data));
          notificationHandler('Anfrage gesendet', 'success');
          resolve();
        })
        .catch((e) => {
          notificationErrorHandler(e);
          reject();
        });
    });
  },
  acceptInquiry({ commit }, inquiryID) {
    const config = getHeader();
    axios.post(`${getRootUrl()}/inquiries/${inquiryID}`, {}, config, { timeout: 180000 })
      .then(() => {
        commit('removeInquiry', inquiryID);
        notificationHandler('Anfrage akzeptiert', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  editInquiry({ commit }, form) {
    const config = getHeader();
    axios.put(`${getRootUrl()}/inquiries/`, form, config)
      .then((response) => {
        const obj = methods.formatInquiry(response.data);

        commit('replaceInquiry', obj);
        notificationHandler('Anfrage bearbeitet', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  deleteInquiry({ commit }, inquiryID) {
    const config = getHeader();
    axios.delete(`${getRootUrl()}/inquiries/${inquiryID}`, config)
      .then(() => {
        commit('removeInquiry', inquiryID);
        notificationHandler('Anfrage gelöscht', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
};

const mutations = {
  setInquiries: (state, inquiries) => { (state.inquiries = inquiries); },
  newInquiry: (state, inquiry) => (state.inquiries.push(inquiry)),
  replaceInquiry: (state, inquiry) => {
    const position = state.inquiries.findIndex((obj) => obj.inquiry.id === inquiry.inquiry.id);
    state.inquiries.splice(position, 1, inquiry);
  },
  removeInquiry: (state, inquiryID) => {
    (state.inquiries = state.inquiries.filter((inquiry) => inquiry.inquiry.id !== inquiryID));
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
