import axios from 'axios';
import {
  getHeader, notificationHandler, notificationErrorHandler, getRootUrl,
} from './util';

const methods = {
  formatArchivedInquiry(obj) {
    const newObj = obj;
    newObj.person.fullName = `${obj.person.name}, ${obj.person.prename}`;
    newObj.person.created_at = this.formatDateTime(new Date(obj.person.created_at));
    newObj.person.updated_at = this.formatDateTime(new Date(obj.person.updated_at));
    newObj.archived_inquiry.created_at = this.formatDateTime(
      new Date(obj.archived_inquiry.created_at),
    );
    newObj.archived_inquiry.updated_at = this.formatDateTime(
      new Date(obj.archived_inquiry.updated_at),
    );
    newObj._rowVariant = this.getRowFormat(newObj.archived_inquiry.action); // eslint-disable-line
    newObj.archived_inquiry.arrivalLocal = this.formatDate(new Date(obj.archived_inquiry.arrival));
    newObj.archived_inquiry.departureLocal = this.formatDate(
      new Date(obj.archived_inquiry.departure),
    );
    newObj.archived_inquiry.formattedRooms = this.formatRooms(obj.archived_inquiry);
    newObj.archived_inquiry.formattedPersonNumbers = this.formatPersonNumbers(obj.archived_inquiry);
    newObj.archived_inquiry.formattedAction = this.formatAction(obj.archived_inquiry.action);
    return newObj;
  },
  getRowFormat(action) {
    let rowVariant = '';

    switch (action) {
      case 'accept':
        rowVariant = 'success';
        break;
      case 'update':
        rowVariant = 'warning';
        break;
      case 'delete':
        rowVariant = 'danger';
        break;
      default:
        rowVariant = '';
    }
    return rowVariant;
  },
  formatDateTime(date) {
    return `${date.toLocaleString()}`;
  },
  formatDate(date) {
    return `${date.toLocaleDateString()}`;
  },
  formatRooms(archivedInquiry) {
    const north = Number(archivedInquiry.roomNorth);
    const south = Number(archivedInquiry.roomSouth);
    let formatedRooms = '';
    if (north && south) {
      formatedRooms = 'Nord, Süd';
    } else if (north) {
      formatedRooms = 'Nord';
    } else if (south) {
      formatedRooms = 'Süd';
    }
    return formatedRooms;
  },
  formatPersonNumbers(archivedInquiry) {
    return Number(archivedInquiry.numberAdults) + Number(archivedInquiry.numberReduced);
  },
  formatAction(action) {
    let formattedAction = '';
    if (action === 'success') {
      formattedAction = 'Akzeptiert';
    } else if (action === 'update') {
      formattedAction = 'Bearbeitet';
    } else if (action === 'delete') {
      formattedAction = 'Gelöscht';
    }
    return formattedAction;
  },
};

/*
eslint no-shadow: ["error", { "allow": ["state"] }]
*/
const state = {
  archivedInquiries: [],
};

const getters = {
  allArchivedInquiries: (state) => state.archivedInquiries,
};

const actions = {
  fetchArchivedInquiries({ commit }) {
    const items = [];
    const config = getHeader();
    axios.get(`${getRootUrl()}/archived_inquiries`, config)
      .then((response) => {
        response.data.forEach((obj) => {
          items.push(methods.formatArchivedInquiry(obj));
        });
        commit('setArchivedInquiries', items);
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  recoverInquiry({ commit }, archivedInquiryID) {
    const config = getHeader();
    axios.post(`${getRootUrl()}/archived_inquiries/${archivedInquiryID}`, {}, config, { timeout: 180000 })
      .then(() => {
        commit('removeArchivedInquiry', archivedInquiryID);
        notificationHandler('Archivierte Anfrage wiederhergestellt', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  deleteArchivedInquiry({ commit }, archivedInquiryID) {
    const config = getHeader();
    axios.delete(`${getRootUrl()}/archived_inquiries/${archivedInquiryID}`, config)
      .then(() => {
        commit('removeArchivedInquiry', archivedInquiryID);
        notificationHandler('Archivierte Anfrage gelöscht', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
};

const mutations = {
  setArchivedInquiries: (state, archivedInquiries) => {
    state.archivedInquiries = archivedInquiries;
  },
  removeArchivedInquiry: (state, archivedInquiryID) => {
    state.archivedInquiries = state.archivedInquiries.filter(
      (archivedInquiry) => archivedInquiry.archived_inquiry.id !== archivedInquiryID,
    );
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
