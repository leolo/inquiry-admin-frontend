import axios from 'axios';
import {
  getHeader, notificationHandler, notificationErrorHandler, getRootUrl,
} from './util';

/*
eslint no-shadow: ["error", { "allow": ["state"] }]
*/
const state = {
  users: [],
};

const getters = {
  getAllUsers: (state) => state.users,
};

const actions = {
  fetchUsers({ commit }) {
    const config = getHeader();
    axios.get(`${getRootUrl()}/users`, config)
      .then((response) => {
        commit('setUsers', response.data);
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  addUser({ commit }, user) {
    const config = getHeader();
    axios.post(`${getRootUrl()}/users/`, user, config)
      .then((response) => {
        commit('newUser', response.data);
        notificationHandler('Benutzer hinzugefügt', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  editUser({ commit }, user) {
    const config = getHeader();
    axios.put(`${getRootUrl()}/users/`, user, config)
      .then((response) => {
        commit('replaceUser', response.data);
        notificationHandler('Benutzer aktualisiert', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
  deleteUser({ commit }, userID) {
    const config = getHeader();
    axios.delete(`${getRootUrl()}/users/${userID}`, config)
      .then(() => {
        commit('removeUser', userID);
        notificationHandler('Benutzer gelöscht', 'success');
      })
      .catch((e) => {
        notificationErrorHandler(e);
      });
  },
};

const mutations = {
  setUsers: (state, users) => { state.users = users; },
  newUser: (state, user) => state.users.push(user),
  replaceUser: (state, user) => {
    const position = state.users.findIndex((obj) => obj.id === user.id);
    state.users.splice(position, 1, user);
  },
  removeUser: (state, userID) => {
    state.users = state.users.filter((user) => user.id !== userID);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
