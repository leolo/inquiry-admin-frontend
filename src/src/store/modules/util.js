import Vue from 'vue';
import Router from '../../router/index';

export function getHeader() {
  let config = '';
  if (Vue.$cookies.get('auth')) {
    const token = Vue.$cookies.get('auth');
    config = {
      headers: { Authorization: `Bearer ${token.access_token}` },
    };
  }
  return config;
}

export function notificationHandler(message, type) {
  Vue.notify({
    group: 'notification',
    title: message,
    type,
    duration: 10000,
  });
}

export function setAuthCookie(response) {
  Vue.$cookies.set(
    'auth',
    JSON.stringify(response.data),
    5400,
    null,
    null,
    null,
    'Strict',
  );
}

export function logOut() {
  Vue.$cookies.remove('auth');
  Router.push('/login');
}

export function notificationErrorHandler(e) {
  const unauthenticatedStatus = 401;
  const unauthorizedStatus = 403;
  let message = '';
  try {
    if (e.response.status === unauthenticatedStatus || unauthorizedStatus) {
      message = 'Failed to authenticate. Please log in again.';
      logOut();
    } else {
      message = e.response.data.detail;
    }
  } catch {
    message = 'An unexpected error occured. Please try later again.';
  }

  notificationHandler(
    message,
    'error',
  );
}

export function getRootUrl() {
  const url = process.env.VUE_APP_API_URL;

  return `${url}/api/v1`;
}

export default {
  getHeader,
  notificationHandler,
  logOut,
};
