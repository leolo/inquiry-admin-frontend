import Vuex from 'vuex';
import Vue from 'vue';
import inquiries from './modules/inquiries';
import archivedInquiries from './modules/archivedInquiries';
import users from './modules/users';
import me from './modules/me';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    inquiries,
    archivedInquiries,
    users,
    me,
  },
});
